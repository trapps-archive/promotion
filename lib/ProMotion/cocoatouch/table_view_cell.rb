class CGRect
  def equals?(frame)
    check = frame.is_a?(CGRect) ? frame : CGRectMake(frame[0][0], frame[0][1], frame[1][0], frame[1][1])

    return CGRectEqualToRect(self, check)
  end
end

module ProMotion
  class TableViewCell < UITableViewCell
    include TableViewCellModule

    attr_accessor :image_size

    # TODO: Is this necessary?
    def layoutSubviews
      super

      # Makes imageView get placed in the corner
      if(self.respond_to?(:imageView) && ! self.imageView.frame.equals?([[0,0],[0,0]]))
        self.imageView.frame = CGRectMake(0, 0, 80, 80)

        if(self.respond_to?(:textLabel))
          new_frame = self.textLabel.frame
          new_frame.origin.x = 100
          self.textLabel.frame = new_frame
        end

        if(self.respond_to?(:detailTextLabel))
          new_frame = self.detailTextLabel.frame
          new_frame.origin.x = 100
          self.detailTextLabel.frame = new_frame
        end
      end

      #self.seperatorInset = UIEdgeInsetsZero #= [0,0,0,0]
      # if self.image_size && self.imageView.image && self.imageView.image.size && self.imageView.image.size.width > 0
      #   f = self.imageView.frame
      #   size_inset_x = (self.imageView.size.width - self.image_size) / 2
      #   size_inset_y = (self.imageView.size.height - self.image_size) / 2
      #   self.imageView.frame = CGRectInset(f, size_inset_x, size_inset_y)
      # end
    end
  end
end
